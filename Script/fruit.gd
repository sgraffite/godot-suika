class_name Fruit extends RigidBody2D

@onready var sprite_2d:Sprite2D = $Sprite2D
@onready var collision_shape_2d:CollisionShape2D = $CollisionShape2D
@onready var animation_player:AnimationPlayer = $AnimationPlayer

var fruit_resource:FruitResource = null : set = set_fruit_resource
var activated:bool = false

func _ready() -> void:
	if(!self.fruit_resource):
		print("Error: fruitResource not defined in _ready()")
		return
	
	sprite_2d.texture = load(fruit_resource.sprite_path)
	collision_shape_2d.shape = CircleShape2D.new()
	collision_shape_2d.shape.radius = fruit_resource.collision_radius

func _physics_process(_delta):
	if not activated:
		# Fixes bug where it will move at a different speed than the whale but only sometimes
		position.x = 0
		return

	for collider in get_colliding_bodies():
		if(collider is Fruit && collider.get_tier() == get_tier() && collider.activated == true):
			collider.activated = false
			collider.queue_free()
			self.activated = false
			self.queue_free()
			SignalBus.fruit_collided.emit(self, collider)
			break

func set_fruit_resource(_value:FruitResource):
	if(!_value):
		return
	fruit_resource = _value

func get_tier() -> int:
	return self.fruit_resource.tier

func get_diameter() -> int:
	return self.fruit_resource.diameter

func flip_sprite(value:bool) -> void:
	sprite_2d.flip_h = value
