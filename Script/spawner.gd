class_name Spawner extends CharacterBody2D

@export var is_player_controlled = false
@export var speed:int = 100
@export var spawn_interval_min:float = .5
@export var spawn_interval_max:float = 1
var FRUIT_SCENE:PackedScene = preload("res://Scene/fruit.tscn")
@export var items:Array[FruitResource] = []
var spawnable_items:Array[FruitResource] = []
const DIRECTIONS = ["left", "right", "up", "down"]
const X_MIN:int = 96
const X_MAX:int = 224
var held_directions = []
@onready var sprite:Sprite2D = %Sprite2D
@onready var collide_sound:AudioStreamPlayer = %AudioStreamPlayer
var queued_fruit:Fruit = null
var is_dragging:bool = false
var is_flipped:bool = false
var target_position:Vector2 = Vector2.ZERO:
	set(value):
		target_position = value.round()

func _ready():
	SignalBus.fruit_collided.connect(fruit_collided)
	# Only tiers 1 through 5 inclusive may spawn
	for res in items:
		var item:Fruit = FRUIT_SCENE.instantiate()
		item.fruit_resource = res
		print("res.tier ", res.tier )
		print("item.get_tier() ", item.get_tier() )
		if(item.get_tier() <= 5):
			spawnable_items.push_front(res)
	
	print("spawnable_items", spawnable_items)
	queue_something()

func _get_movement(dir):
	var vectors = {
		"left": Vector2(-speed, 0),
		"right": Vector2(speed, 0),
		"up": Vector2(0, 0),
		"down": Vector2(0, 0)
	}
	return vectors[dir]

func _input(event) -> void:
	if(!is_player_controlled):
		return
	
	handle_wasd()
	if(Input.is_action_just_pressed("ui_select")):
		drop_fruit()
	
	if(event is InputEventMouseButton):
		if(event.button_index == MOUSE_BUTTON_RIGHT):
			drop_fruit()
		
		if(event.button_index == MOUSE_BUTTON_LEFT):
			target_position = Vector2(event.position.x, position.y)
			is_dragging = true if event.pressed else false
	elif event is InputEventMouseMotion and is_dragging:
		# Update the target position as the mouse moves
		target_position = Vector2(event.position.x, position.y)

func _process(_delta: float) -> void:
	if(target_position == Vector2.ZERO):
		return
	
	if(abs(global_position.x - target_position.x) <= abs(target_position.x) * 0.01):
		target_position = Vector2.ZERO
		print("position reached")
		return

	velocity = (target_position - global_position).normalized() * speed
	
	#Face sprites the correct way
	var should_be_flipped:bool = velocity.x > 0
	if(is_flipped != should_be_flipped):
		is_flipped = should_be_flipped
		sprite.set_flip_h(is_flipped)
		if(queued_fruit):
			queued_fruit.flip_sprite(is_flipped)
	
	move_and_slide()
	
	# Keep whale in the box
	if(position.x < X_MIN):
		position.x = X_MIN
		target_position.x = X_MIN
	elif(position.x > X_MAX):
		position.x = X_MAX
		target_position.x = X_MAX

func handle_wasd():
	var is_released:bool = false
	for dir in DIRECTIONS:
		var is_pressed = Input.is_action_pressed("ui_" + dir)
		if(Input.is_action_just_released("ui_" + dir)):
			is_released = true
		var direction_index = held_directions.find(dir)
		if(direction_index == -1):
			if(is_pressed):
				held_directions.push_front(dir)
		else:
			if(!is_pressed):
				held_directions.remove_at(direction_index)
	
	if(held_directions.size() > 0):
		target_position = global_position + (_get_movement(held_directions[0]) * speed)
	elif(is_released):
		target_position = position
		
func drop_fruit():
	if(queued_fruit == null):
		return
	
	# Change parent of queue fruit so it does not follow the whale
	queued_fruit.position.x = position.x
	remove_child(queued_fruit)
	get_parent().add_child(queued_fruit)
	queued_fruit.gravity_scale = 1
	queued_fruit.z_index = 1
	
	#Keep velocity of user input
	if(target_position != Vector2.ZERO):
		queued_fruit.linear_velocity = self.velocity
	
	queued_fruit.activated = true
	queued_fruit.set_contact_monitor(true)
	queued_fruit.set_max_contacts_reported(10)
	# Turn on the CollisionShape2D
	for child in queued_fruit.get_children():
		if(child is CollisionShape2D):
			child.disabled = false
		if(child is AudioStreamPlayer):
			print("tier = " + str(queued_fruit.get_tier()), " pitch = ", get_pitch_scale(queued_fruit.get_tier()))
			#print("tier = " + str(queued_fruit.get_tier()))
			var pitch_scale:float = get_pitch_scale(queued_fruit.get_tier())
			print("pitch_scale = " + str(pitch_scale))
			child.pitch_scale = pitch_scale
			child.play()
	#Dequeue the fruit
	queued_fruit = null
	var timer = get_tree().create_timer(randf_range(spawn_interval_min, spawn_interval_max))
	timer.timeout.connect(queue_something)
	
func fruit_collided(fruitA:Fruit, fruitB:Fruit):
	print("Fruit "+str(fruitA.get_rid())+" collided with "+str(fruitB.get_rid())+" tier[" + str(fruitA.get_tier()) + "]!")
	print("collide pitch", collide_sound.pitch_scale)
	collide_sound.pitch_scale = 1.0 + get_random_pitch_adjustment()
	collide_sound.play()
	SignalBus.point_counted.emit(fruitA.get_tier() * 2)
	#print("FruitA ang velocity = " + str(fruitA.angular_velocity) + "/ lin velocity = " + str(fruitA.linear_velocity))
	#print("FruitB ang velocity = " + str(fruitB.angular_velocity) + "/ lin velocity = " + str(fruitB.linear_velocity))
	spawn_specific([fruitA, fruitB], fruitA.get_tier() + 1)

func queue_something():
	var res = spawnable_items.pick_random()
	queued_fruit = FRUIT_SCENE.instantiate()
	queued_fruit.fruit_resource = res
	print("spawning tier["+str(queued_fruit.get_tier())+"] fruit!")
	# Attaching to the spawner allows the player to move the fruit
	queued_fruit.position.y = position.y + queued_fruit.get_diameter()/2.0
	queued_fruit.gravity_scale = 0
	queued_fruit.z_index = 1
	add_child(queued_fruit)

func spawn_specific(fruits:Array[Fruit], tier:int):
	var fruit:Fruit = fruits[0]
	#var linearVelocity = fruits[0].linear_velocity if fruits[0].linear_velocity.length() >= fruits[1].linear_velocity.length() else fruits[1].linear_velocity
	var angularVelocity:float = maxf(fruits[0].angular_velocity, fruits[1].angular_velocity)
	var location:Vector2 = fruit.get_position()
	print("spawn_specific tier["+str(tier)+"] fruit!")
	for res:FruitResource in items:
		if(res.tier != tier):
			#item.queue_free()
			continue
		
		var item:Fruit = FRUIT_SCENE.instantiate()
		item.fruit_resource = res
		item.position.x = location.x
		item.position.y = location.y
		item.angular_velocity = angularVelocity
		item.linear_velocity = fruit.linear_velocity
		item.activated = true
		item.set_contact_monitor(true)
		item.set_max_contacts_reported(10)
		# Turn on the CollisionShape2D
		for child in item.get_children():
			if(child is CollisionShape2D):
				child.disabled = false

		get_parent().add_child(item)
		break

# Random pitch adjustment between -0.1 and +0.1
func get_random_pitch_adjustment() -> float:
	return randf() * 0.2 - 0.1

func get_pitch_scale(tier: int) -> float:
	var weight:float = float(tier - 1) / (spawnable_items.size() - 1)
	var clamped_weight:float = clamp(weight, 0.1, 0.8)  # Clamp to 0.1 - 0.8
	#print("normalized_tier = ", weight)
	return lerp(1.0, 0.1, clamped_weight + get_random_pitch_adjustment())
