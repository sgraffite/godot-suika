extends Node

@warning_ignore("unused_signal")
signal game_start()

@warning_ignore("unused_signal")
signal game_over(score:int)

@warning_ignore("unused_signal")
signal main_menu(value:bool)

@warning_ignore("unused_signal")
signal point_counted(value:int)

@warning_ignore("unused_signal")
signal fruit_collided(fruitA:Fruit, fruitB:Fruit)

@warning_ignore("unused_signal")
signal game_quit()
