extends Node2D
@onready var score_label = %ScoreLabel
@onready var high_score_label: Label = %HighScoreLabel
@onready var spawner = %Spawner
@onready var fruit_out_of_bounds_label: Label = %FruitOutOfBoundsLabel

var high_score = 0 : set = _set_high_score
var score = 0 : set = _set_score
var fruit_oob = 0 : set = _set_fruit_oob
const MAX_FRUIT_OOB:int = 4

# Called when the node enters the scene tree for the first time.
func _ready():
	fruit_out_of_bounds_label.hide()
	SignalBus.point_counted.connect(_on_fruit_combined)
	print("high_score_label", high_score_label)
	high_score = high_score
	
func _set_score(value):
	score = value
	if(!score_label):
		return
	
	score_label.text = "Score: " + str(score)
	
func _set_high_score(value):
	high_score = value
	if(!high_score_label):
		return
	
	high_score_label.text = "High Score: " + str(high_score)
	
func _set_fruit_oob(value):
	if(!fruit_out_of_bounds_label):
		return
	
	fruit_oob = value
	if(fruit_oob > 1):
		fruit_out_of_bounds_label.show()
	else:
		fruit_out_of_bounds_label.hide()
	
	fruit_out_of_bounds_label.text = "Out of bounds: " + str(fruit_oob)
	if(fruit_oob > MAX_FRUIT_OOB):
		game_over()

func _on_fruit_combined(value):
	score = score + value

#Detect fruit in out of bounds
func _on_area_2d_body_entered(_body:Fruit):
	if(_body):
		_body.animation_player.play("new_animation")
		fruit_oob = fruit_oob + 1

func _on_area_2d_body_exited(_body:Fruit):
	if(_body):
		_body.animation_player.stop()
		fruit_oob = fruit_oob - 1

func game_over():
	spawner.is_player_controlled = false
	SignalBus.game_over.emit(score)
