extends Control

@onready var main_menu: CanvasLayer = $MainMenu
@onready var level_bucket: Control = $LevelBucket
@onready var start_button: Button = %StartButton
@onready var quit_button: Button = %QuitButton

const GAME_OVER_POPUP = preload("res://Scene/game_over_popup.tscn") as PackedScene
const WORLDMAP = preload("res://Scene/worldmap.tscn") as PackedScene
var current_level:Node = null
var high_score:int = 0
var is_main_menu_open:bool:
		set(value):
			is_main_menu_open = value
			if(is_main_menu_open):
				main_menu.show()
			else:
				main_menu.hide()

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	is_main_menu_open = true
	SignalBus.main_menu.connect(listen_main_menu)
	start_button.button_down.connect(start_game)
	quit_button.button_down.connect(quit_game)
	SignalBus.game_start.connect(start_game)
	SignalBus.game_over.connect(game_over)

func _input(_event) -> void:
	if Input.is_action_just_pressed("ui_cancel"):
		print("ui_cancel")
		is_main_menu_open = !is_main_menu_open
		set_process(!is_main_menu_open)

func start_game() -> void:
	print("starting")
	print(str(SignalBus.game_start))
	
	start_button.disabled = true
	is_main_menu_open = false
	if(current_level != null):
		current_level.free()
	current_level = WORLDMAP.instantiate()
	current_level.high_score = high_score
	level_bucket.add_child(current_level)

func listen_main_menu(value:bool) -> void:
	is_main_menu_open = value

func game_over(score:int):
	print("score = " + str(score))
	if(score > high_score):
		high_score = score
	var popup = GAME_OVER_POPUP.instantiate()
	popup.score = score
	get_tree().root.add_child(popup)

func quit_game() -> void:
	print("quitting")
	get_tree().quit()
