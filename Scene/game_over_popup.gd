class_name GameOverPopup extends AcceptDialog

@onready var game_over_score_label: Label = %GameOverScoreLabel
var score:int
var ok_button:Button

func _ready():
	ok_button = get_ok_button()
	ok_button.disabled = true
	game_over_score_label.text = "Final Score: " + str(score)
	var timer = get_tree().create_timer(1.5)
	timer.timeout.connect(allow_ok)

func allow_ok():
	ok_button.disabled = false
#
func _on_confirmed() -> void:
	print("_on_confirmed")
	queue_free()
	SignalBus.game_start.emit()


func _on_canceled() -> void:
	print("_on_canceled")
	queue_free()
	SignalBus.game_start.emit()
