class_name FruitResource extends Resource

@export var tier:int = 0
@export var diameter:int = 0
@export var sprite_path:String = "res://Sprite/fruit01.png"
@export var collision_radius:int = 0
